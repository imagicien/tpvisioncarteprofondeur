#include <cassert>
#include <algorithm>
#include "carteProfondeur.h"

carteProfondeur::carteProfondeur()
{
}

carteProfondeur::~carteProfondeur()
{
	std::cout << "\nCIAO LOL kk zzzzzz je dors\n";
}

inline size_t upperPowerOfTwo(size_t orig_size)
{
	size_t new_size = 1;
	while (new_size < orig_size)
		new_size *= 2;
	return new_size;
}

Image carteProfondeur::getCarteProfondeur(Image& nom_gauche,Image& nom_droite)
{
	// S'assurer que les deux images sont de meme taille
	assert( m_ImGauche.GetWidth() == m_ImDroite.GetWidth() &&
		    m_ImGauche.GetWidth() == m_ImDroite.GetHeight() );

	// Travailler des images carrees, de dimensions etant des puissances de deux
	size_t largeur = upperPowerOfTwo(nom_gauche.GetWidth());
	size_t hauteur = upperPowerOfTwo(nom_droite.GetHeight());
	size_t cote = (largeur > hauteur) ? largeur : hauteur;
	
	// redimmensionner les images
	//m_ImGauche.crop(0, 0, cote - 1, cote - 1);
	//m_ImDroite.crop(0, 0, cote - 1, cote - 1);
	Image result = resize(nom_gauche, largeur, hauteur);
	//genererHierarchie();
	return result;
}

Image carteProfondeur::resize(Image& in_image, int width, int height)
{
	Image image_resized = Image(width, height, in_image.GetNumBands());
	for (int i = 0; i < in_image.GetWidth(); i++)
	{
		for (int j = 0; j < in_image.GetWidth(); j++)
		{
			for (int b = 0; b < image_resized.GetNumBands(); b++)
			{
				image_resized[b](i, j) = in_image[b](i, j);                                            
			}
		}
	}

	return image_resized;
}

Coord carteProfondeur::miseEnCorrespondance(Coord pixel_gauche)
{
	assert(m_HierarchieGauche.size() > 0 && m_HierarchieDroite.size() > 0);

	int niveau(m_HierarchieGauche.size()-1);
	Coord coin_region_droite = Coord{ 0, 0 }; // Coin sup�rieur gauche de la region 2x2
	Coord pixel_gauche_niveau_courant;
	Coord pixel_droit_trouve;

	// ###TODO: TRAITER CAS INITIAL OU LA REGION EST PLUS QUE 2x2

	while (niveau >= 0)
	{
		pixel_gauche_niveau_courant = pixel_gauche / pow(2, niveau);
		
		pixel_droit_trouve = correlation(pixel_gauche_niveau_courant, coin_region_droite, 0);

		// A quelle region correspond ce pixel droit?
		coin_region_droite.x = pixel_droit_trouve.x * 2;
		coin_region_droite.y = pixel_droit_trouve.y * 2;

		niveau--;
	}

	return pixel_droit_trouve;
}

void carteProfondeur::genererHierarchie()
{
	/*size_t cote(SIZE_MIN);

	std::cout << "Grande taille: " << m_ImGauche.width() << std::endl;

	while (cote < m_ImGauche.width())
	{
		std::cout << cote << std::endl;
		m_HierarchieGauche.push_back(m_ImGauche.get_resize(cote, cote, 1, 3, 3));
		m_HierarchieDroite.push_back(m_ImDroite.get_resize(cote, cote, 1, 3, 3));
		cote *= 2;
	}

	m_HierarchieGauche.push_back(m_ImGauche);
	m_HierarchieDroite.push_back(m_ImDroite);
*/
	//for (size_t i = 0; i < m_HierarchieDroite.size(); i++)
	//{
	//	m_HierarchieDroite[i].display();
	//}



}

Coord carteProfondeur::correlation(Coord pixel_gauche, Coord region_droite, int in_niveau)
{
	// Pour le cas de la plus petite image, on effectu une  recherche dans toute l'image

	Image& imageD = m_HierarchieDroite[in_niveau];
	Image& imageG = m_HierarchieGauche[in_niveau];
	Coord meilleur_coord = Coord{-1, -1}; 
	float meilleur_simil = -numeric_limits<float>::infinity();

	// TODO : -------------------> GESTION DU CAS DE DEPART <-------------------
	if (in_niveau == 0)
	{
	//	for (int i = 0; i < imageD.width(); i++)
	//	{
	//		for (int j = 0; j < imageD.height(); j++)
	//		{
	//			//float simi = similarite_fenetre(pixel_gauche)
	//		}
	//	}
	}

	// Sinon on considere seulement un voisinage de 2x2
	else	
	{
		for (int i = region_droite.x; i <= region_droite.x + 1; i++)
		{
			for (int j = region_droite.y; j <= region_droite.y + 1; j++)
			{
				float simi = similarite_fenetre(pixel_gauche, Coord{i,j}, imageG, imageD);
				if (simi > meilleur_simil)
				{
					meilleur_coord = Coord{ i, j };
					meilleur_simil = simi;
				}
			}
		}
	}


	return Coord{0,0};
}

float carteProfondeur::similarite_fenetre(Coord& pixel_gauche, Coord& region_droite, const Image& imgGauche, const Image& imgDroite)
{
	// TODO : -------------------> Gestion des bordures <-------------------
	int departX = max(0, region_droite.x - 2);
	int departY = max(0, region_droite.y - 2);
	int finX = min(imgDroite.GetWidth() - 1, region_droite.x + 2);
	int finY = min(imgDroite.GetHeight() - 1, region_droite.y + 2);

	float simi = 0;

	for (int i = departX; i <= finX; i++)
	{
		for (int j = departY; j <= finY; j++)
		{
			//simi += similarite()
		}
	}
	return 0;
}

float carteProfondeur::similarite(Coord& pixel_gauche, Coord& region_droite, const Image& imgGauche, const Image& imgDroite)
{
	return 0;
}


