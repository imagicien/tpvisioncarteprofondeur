#include <string>
#include <iostream>
#include <stdio.h>
#include "image.h"
#include "fichierIm.h"

using namespace std;


/*****************************************************************************
* Lit un fichier jusqu'� la prochaine ligne ou espace
/*****************************************************************************/
static bool ReadToken(
	char*	o_str, 
	int&	o_nStrlen, 
	FILE*	i_pfile
)
{
	int	i(0);
	
	do{
		if(fread(o_str,1,1,i_pfile)!=1)
			return false;
	} while((o_str[i]==10) || (o_str[i]==13) || (o_str[i]==32));

	if (o_str[i] != '#'){		
		do {
			i++;		
			if(fread(o_str+i,1,1,i_pfile)!=1)
				return false;
		} while((o_str[i] != 10) && (o_str[i] != 13) && (o_str[i] != 32));
	}
	else
		i++;
	
	o_str[i] = 0;
	o_nStrlen = i;

	return true;
}

/*****************************************************************************
 * Lit un fichier jusqu'� la prochaine ligne
 *****************************************************************************/
static bool ReadString(
	char*	o_str, 
	int&	o_nStrlen, 
	FILE*	i_pfile
)
{
	int	i(-1);

	do{
		i++;
		if (fread(o_str+i, 1, 1, i_pfile) != 1)
			return false;
	} while ((o_str[i]!=10) && (o_str[i]!=13));

	o_str[i] = 0;
	o_nStrlen = i;

	return true;
}


/*****************************************************************************
 * Lecture du fichier PGM Ascii
 * dans la structure Image
 *****************************************************************************/

static bool ReadAscImg(
	Image&		o_imgDst,
	FILE*		i_pfile
)
{
	char	pcTempBuf[100];

	int		nSize(o_imgDst.GetWidth()*o_imgDst.GetHeight()),
			nBand(o_imgDst.GetNumBands()), 
			nStrlen,
			nImgPos, 
			i;

	for (nImgPos=0;  nImgPos<nSize; nImgPos++){
		for (i=0;  i<nBand; i++){
			if (ReadToken(pcTempBuf, nStrlen, i_pfile))
				o_imgDst[i](nImgPos) = (PIXTYPE) atoi(pcTempBuf);
			else 
				return false;
		}
	}

	return true;
}

/*****************************************************************************
 * Lecture du fichier PGM Binaire (1 bit par pixel)
 * dans la structure Image
 *****************************************************************************/

static bool ReadBWBinImg(
	Image&		o_imgDst,
	FILE*		i_pfile
)
{
	unsigned char	*pucBuf;

	int				i, j, k, 
					nWidthOctets,
					nWidth(o_imgDst.GetWidth()), 
					nHeight(o_imgDst.GetHeight()),
					nImgPos(0);

	bool			bCode(true);
	
	nWidthOctets = (nWidth%8 ? (nWidth/8)+1 : nWidth/8);

	//  allocation de la memoire pour un tampon de lecture au fichier 
	pucBuf = new unsigned char[nWidthOctets];

	if(pucBuf  == NULL){
		// Allocating file buffer failed
		return false;
	}

	//  lecture du tampon in_Height fois 
	for (j=0; j<nHeight; j++){
		if (fread(pucBuf, nWidthOctets, 1, i_pfile) != 1){
			if (feof(i_pfile) != 0) {
				// Prematured end of file
				bCode = false;
				break;
			}
			else{
				// Cannot read file
				bCode = false;
				break;
			}
		}
		else{
			unsigned char ucMask;

			for (i=0; i<(nWidth/8); i++){
				ucMask = 128;

				for (k=0; k<8; k++, nImgPos++){
					o_imgDst[0](nImgPos) = (PIXTYPE) ((pucBuf[i]&ucMask) ? 0 : 255);
					ucMask = ucMask>>1;
				}
			}
			ucMask = 128;

			for (k=0; k<(nWidth%8); k++, nImgPos++){
				o_imgDst[0](nImgPos) = (PIXTYPE) ((pucBuf[i]&ucMask) ? 0 : 255);
				ucMask = ucMask>>1;
			}
		}
		
	}

	delete[] pucBuf;					// desallocation du tampon 

	return bCode;
}

/*****************************************************************************
 * Lecture du fichier PGM Binaire (1 bit par pixel)
 * dans la structure Image
 *****************************************************************************/

static bool ReadBinImg(
	Image&		o_imgDst,
	FILE*		i_pfile
)
{
	int				nBands(o_imgDst.GetNumBands()),
					i,j,k,
					nWidth(o_imgDst.GetWidth()), 
					nHeight(o_imgDst.GetHeight()),
					nImgPos(0);

	unsigned char	*pucBuf;

	bool			bCode(true);

	//  allocation de la memoire pour un tampon de lecture au fichier 
	pucBuf = new unsigned char[nWidth*nBands];

	if (pucBuf  == NULL)
		// Allocating file buffer failed
		return false;

	//  lecture du tampon in_Height fois 
	for (j=0; j<nHeight; j++) {
		if (fread(pucBuf, nWidth*nBands, 1, i_pfile) != 1){
			if (feof(i_pfile) != 0){
				// Prematured end of file
				bCode = false;
				break;
			}
			else{
				// Cannot open file 
				bCode = false;
				break;
			}
		}
		else {
			for (i=0; i<nWidth; i++, nImgPos++)
				for (k=0; k<nBands; k++)
					o_imgDst[k](nImgPos) = pucBuf[i*nBands+k];
		}
		
	}

	delete [] pucBuf;					// desallocation du tampon

	return bCode;
}

bool fichierIm::charge() {
	std::cout<<"le chargement de l'image s'execute..."<<std::endl;

	FILE	*pfile;
	
	bool	bCode(true);
	
	char	pcTempBuf[COMMENTBUFMAX],
			cType;

	const char	*fileName= m_nom.c_str();

	int		nStrleng,
			nWidth, nHeight, 
			nMaxval,
			nBands;
	
	if ((pfile=fopen(fileName, "rb")) == NULL){
		std::cout<<"Can't open file "<<m_nom<<std::endl;
		return false;
	}

	// Read PGM info header
	if (!ReadString(pcTempBuf, nStrleng, pfile))
		return false;

	if ((pcTempBuf[0]!='P')
	|| ((pcTempBuf[1]!='2') && (pcTempBuf[1]!='5') && (pcTempBuf[1]!='3')
		&& (pcTempBuf[1]!='6') && (pcTempBuf[1]!='4'))){
			std::cout<<"Bad file type "<<pcTempBuf[0]<<pcTempBuf[1]<<std::endl;
			return false;
		}
		
	cType = pcTempBuf[1];

	do{
		ReadToken(pcTempBuf, nStrleng, pfile);
		if (pcTempBuf[0]=='#') 
			ReadString(pcTempBuf+1, nStrleng, pfile);
	} while (pcTempBuf[0]=='#');
	nWidth = atoi(pcTempBuf);

	do{
		ReadToken(pcTempBuf, nStrleng, pfile);
		if (pcTempBuf[0]=='#') 
			ReadString(pcTempBuf+1, nStrleng, pfile);
	} while(pcTempBuf[0]=='#');
	nHeight = atoi(pcTempBuf);
	
	if (cType != '4'){
		do{
			ReadToken(pcTempBuf, nStrleng, pfile);
			if (pcTempBuf[0]=='#') 
				ReadString(pcTempBuf+1, nStrleng, pfile);
		} while(pcTempBuf[0]=='#');
		nMaxval = atoi(pcTempBuf);
	}
	
	switch (cType){
		case '2': case '5': case '4': {
			nBands = 1;	
			break;
		}
		case '3': case '6': {	
		  	nBands = 3;
			break;
		}
		default: {
			return false;
		}
	}
	
	if (nWidth != m_img.GetWidth() || nHeight != m_img.GetHeight()
	|| nBands != m_img.GetNumBands())
		if (!m_img.Resize(nWidth, nHeight, nBands))
			return false;
	
	switch (cType){
		case '2':
		case '3':{ 
			bCode = ReadAscImg(m_img, pfile); 
			break; 
		}
		case '4':{ 
			bCode = ReadBWBinImg(m_img, pfile); 
			break; 
		}
		case '5':
		case '6':{ 
			bCode = ReadBinImg(m_img, pfile); 
			break; 
		}
	}
			
	fclose(pfile);						// fermeture du fichier
	
	return bCode;
}

bool fichierIm::sauvegarde() const{
	std::cout<<"la sauvegarde de l'image s'execute..."<<std::endl;

	unsigned char	*pucBuf;

	FILE			*pfile;

	char			pcSignedBuf[COMMENTBUFMAX];

	int				b, x, y, 
					nPosBuf,
					nWidth(m_img.GetWidth()), 
					nHeight(m_img.GetHeight()), 
					nBands(m_img.GetNumBands());

	bool			bWriteOK;

	const char	*in_strFilename= m_nom.c_str();

	if (!((nBands == 1) || (nBands == 3))) return false;

	//  allocation de la memoire pour un tampon de lecture au fichier 
	pucBuf = new unsigned char[nWidth*nBands];

	if (!pucBuf){
		// Allocating file buffer failed
		return false;
	}	

	// ouverture du fichier   
	if ((pfile=fopen(in_strFilename, "wb")) != NULL){
		bWriteOK = true;

		if (nBands == 1)
			snprintf(pcSignedBuf, COMMENTBUFMAX, "P5\n# This file was created by AnalyseThis\n");
		else 
			snprintf(pcSignedBuf, COMMENTBUFMAX, "P6\n# This file was created by AnalyseThis\n");
			
		if (fwrite(pcSignedBuf, strlen(pcSignedBuf), 1, pfile) != 1){
			// Writing to file failed
			bWriteOK = false;
		}

//		snprintf(pcSignedBuf, COMMENTBUFMAX, "%d %d\n%d\n\0", nWidth, nHeight, 255);
		snprintf(pcSignedBuf, COMMENTBUFMAX, "%d %d\n%d\n", nWidth, nHeight, 255);
		if (fwrite(pcSignedBuf, strlen(pcSignedBuf), 1, pfile) != 1)
			// Writing to file failed
			bWriteOK = false;

		for (y=0; (y<nHeight) && bWriteOK; y++){
			nPosBuf = 0;

			for (x=0; x<nWidth; x++) {
				for (b=0; b<nBands; b++)
					pucBuf[nPosBuf++] = (unsigned char) (m_img[b](x, y)+0.5f);
			}  
			
			if (fwrite(pucBuf, nWidth*nBands, 1, pfile) != 1)
				// Writing to file failed
				bWriteOK = false;
		
		}
		
		fclose(pfile);						// fermeture du fichier                 
	}
	else 
		// Cannot open file 
		bWriteOK = false;

	delete [] pucBuf;					// deallocation de la memoire du tampon 	

	return bWriteOK;    
}

