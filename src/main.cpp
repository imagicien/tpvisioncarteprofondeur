/*******************************************************************************
Programme d'execution des TPs du cours d'Analyse d'images - IMN259

Ce petit programme doit permettre de faire l'execution des travaux
pratiques qui seront realises par les etudiants cours d'analyse d'images.

Programme realise par Philippe Dompierre
Ete 2005, termine le 24 aout 2005.
*******************************************************************************/

#include <fstream>
#include <string>
#include <cctype>
#include <vector>
#include <stdio.h>
#include "fichierIm.h"
#include "fonctions.h"
#include "points.h"
#include "TabonneCornerDetect.h"

using namespace std;

// Entete des fonctions de l'interface

bool calibrerCamera(const vector<fichierIm>& i_vImgs);
int  choixImages(const vector<fichierIm>&);
void detecterJonctions(vector<fichierIm>& io_vImgs);
bool ecrireCalibFile(const calibParam& i_param, const string& i_strNomFichier);
void enregistrerImage(vector<fichierIm>&);
bool enregistrerImage(fichierIm&);
bool lireCalib(calibParam& param);
bool lireCalibFile(calibParam& out_param, const string& in_strNomFichier);
bool lirePtsScene(vector<point3d>& o_vPts);
bool lirePtsSceneFile(vector<point3d>& o_vPts, const string& i_strNomFichier);
void listerImages(const vector<fichierIm>&);
void ouvrirImage(vector<fichierIm>&);
bool ouvrirImage(fichierIm&,std::string);
bool rectification(const vector<fichierIm>& i_vImgs);
string removeExt(string i_strBaseFileName);
void seuiller(const vector<fichierIm>& i_vImgs);
bool stereovision(vector<fichierIm>& i_vImgs);

#define MAX_IMAGES 20
#define NONVALIDE '0'


/************************************************************************************
bool confirmerArret()

Entree: aucune
Sortie: un booleen, true ou false
Retour: un booleen, true ou false selon le choix de l'usager
Resultat: si l'usager confirme sa sortie du programme on retourne true, sinon false

Fonction ne servant simplement qu'a servir a confirmer la sortie du programme par
l'usager. Affichage d'un avertissement a l'usager que les images sont sauvegardees
seront perdues. Elle rredemande a l'usager la confirmation si celui-ci n'a pas fait
un choix possible. Retourne true ou false selon le choix de l'usager.
************************************************************************************/

bool confirmerArret()
{
	char cValide;

	do
	{
		cout << "ATTENTION!, les images n'ayant pas ete sauvegardees plus tot seront perdues." << endl
			<< "Etes-vous certain de vouloir quitter l'application des maintenant? (O/N): ";

		cin >> cValide;
		cValide = toupper(cValide);

		if (cValide != 'O' && cValide != 'N')
			cout << "Confirmer (O/N)." << endl;
	} while (cValide != 'O' && cValide != 'N');

	return (cValide == 'O');
}


/******************************************************************************
bool enregistrerImage(fichierIm& i_imgOrig)

Entr�e:		i_imgOrig : image � sauvegarder
Sortie:		aucune
Retour:		vrai si l'enregistrement sur disque s'effectue bien

Fonction qui effectue l'enregistrement d'une image sur le disque. La fonction
demande � l'usager le nom du fichier � �crire et s'il veut un changement de
dynamique et lequel.
******************************************************************************/

bool enregistrerImage(fichierIm& i_imgOrig)
{
	char cChoix;
	fichierIm imgTemp;
	string strNomFichier;

	do {
		cout << endl << "Nom du fichier a sauvegarder (avec extension) => ";
		cin >> strNomFichier;

		fstream osOrig(strNomFichier.data(), ios::in);  // v�rifier si le fichier existe d�j�

		if (!!osOrig) {
			cout << endl << "ATTENTION : Fichier existant" << endl
				<< "La sauvegarde effacera definitivement l'ancien fichier" << endl;
			do {
				cout << "Desirez-vous poursuivre ? (O/N): ";

				cin >> cChoix;
				cChoix = toupper(cChoix);

				if (cChoix != 'O' && cChoix != 'N')
					cout << endl << "Choix non valide" << endl;

			} while (cChoix != 'O' && cChoix != 'N');

		}
		else {
			cChoix = 'O';
		}

		osOrig.close();


	} while (cChoix != 'O');

	do {
		cout << endl << "Changement de dynamique " << endl
			<< " A - Aucun" << endl
			<< " I - Lineaire" << endl
			<< " O - Logarithmique" << endl
			<< " Choix => ";
		cin >> cChoix;
		cChoix = toupper(cChoix);

		switch (cChoix){
		case 'A':
			imgTemp = i_imgOrig;
			break;
		case 'I':
			if (!changementDynamique(imgTemp.image(), i_imgOrig.image()))
				return false;
			cChoix = 'A';
			break;
		case 'O':
			if (!changementDynamique(imgTemp.image(), i_imgOrig.image(), DLOG))
				return false;
			cChoix = 'A';
			break;
		default:
			cout << endl << "Choix non valide" << endl;
		}

	} while (cChoix != 'A');

	imgTemp.nom() = strNomFichier;
	return imgTemp.sauvegarde();

}

/**********************************************************************************
bool ouvrirImage(fichierIm& o_fim)

Entr�e: aucune
Sortie: o_fim : image lue
Retour: boul�en, vrai si le chargement s'effectue bien

Cette fonction demande � l'usager le nom de l'image qu'il d�sire ouvrir. Si le
chargement s'effectue correctement, l'image est retourn�e par r�f�rence
**********************************************************************************/

bool ouvrirImage(fichierIm& o_fim,std::string in_string)
{
	string strNomFichier;

	cout << endl << "-----------> Ouverture d'une image <-----------" << endl
		<< endl << "Nom de l'image (avec extension) => ";
	/*cin >> strNomFichier;*/

	o_fim.nom() = in_string;

	return o_fim.charge();

}

/**********************************************************************************
bool lirePtsSceneFile(vector<point3d>& o_vPts, const string& i_strNomFichier)

Entr�e: Nom du fichier � lire
Sortie: Vecteur des points de la sc�ne
Retour: boul�en, vrai si le chargement s'effectue bien

Cette fonction lit le fichier sous le nom "i_strNomFichier", les points 3D de la sc�ne sous forme de vecteur
**********************************************************************************/
bool lirePtsSceneFile(vector<point3d>& o_vPts, const string& i_strNomFichier){
	ifstream	file;
	point3d	pt;

	file.open(i_strNomFichier.c_str());

	if (!file){
		std::cout << "Can't open file " << i_strNomFichier << std::endl;
		return false;
	}

	while (file){
		file >> pt(X);
		if (file){
			file >> pt(Y); file >> pt(Z);
			o_vPts.push_back(pt);
		}
	}

	cout << "Il y a " << o_vPts.size() << " points dans le fichier" << endl;

	return true;
}

/**********************************************************************************
bool ecrireCalibFile(const calibParam& i_param, const string& i_strNomFichier)

Entr�e: Param�tres de calibration et nom du fichier � �crire
Sortie: aucune
Retour: boul�en, vrai si le chargement s'effectue bien

Cette fonction sauvegarde sous le nom "i_strNomFichier" les param�tres de calibrage appropri�s
**********************************************************************************/

bool ecrireCalibFile(const calibParam& i_param, const string& i_strNomFichier){
	ofstream	file;

	file.open(i_strNomFichier.c_str());

	if (!file){
		std::cout << "Can't open file " << i_strNomFichier << std::endl;
		return false;
	}

	file << "Ox = " << i_param.m_vecO(X) << endl;
	file << "Oy = " << i_param.m_vecO(Y) << endl;

	file << "Sx = " << i_param.m_vecS(X) << endl;
	file << "Sy = " << i_param.m_vecS(Y) << endl;

	file << "Dimx = " << i_param.m_vecDim(X) << endl;
	file << "Dimy = " << i_param.m_vecDim(Y) << endl;
	file << "Zprime = " << i_param.m_dZprime << endl;

	file << "Rotation = " << endl;
	file << i_param.m_matR(0, 0) << " ";
	file << i_param.m_matR(0, 1) << " ";
	file << i_param.m_matR(0, 2) << " " << endl;

	file << i_param.m_matR(1, 0) << " ";
	file << i_param.m_matR(1, 1) << " ";
	file << i_param.m_matR(1, 2) << " " << endl;

	file << i_param.m_matR(2, 0) << " ";
	file << i_param.m_matR(2, 1) << " ";
	file << i_param.m_matR(2, 2) << " " << endl;

	file << "Translation = " << endl;
	file << i_param.m_vecT(0) << " ";
	file << i_param.m_vecT(1) << " ";
	file << i_param.m_vecT(2) << " " << endl;

	file.close();

	return true;
}

/**********************************************************************************
bool lireCalibFile(calibParam& out_param, const string& in_strNomFichier)

Entr�e: Nom du fichier � lire
Sortie: Param�tres de calibration
Retour: boul�en, vrai si le chargement s'effectue bien

Cette fonction lit le fichier sous le nom "i_strNomFichier", les param�tres de calibrage appropri�s
**********************************************************************************/
bool lireCalibFile(calibParam& out_param, const string& in_strNomFichier){
	string strDummy;
	ifstream	file;
	char cDummy;

	file.open(in_strNomFichier.c_str());

	if (!file){
		std::cout << "Can't open file " << in_strNomFichier << std::endl;
		return false;
	}

	while (strDummy.compare("u0")){
		file >> strDummy;
	}
	file >> strDummy;
	file >> out_param.m_vecO(X);

	while (strDummy.compare("v0")){
		file >> strDummy;
	}
	file >> strDummy;
	file >> out_param.m_vecO(Y);

	while (strDummy.compare("au")){
		file >> strDummy;
	}
	file >> strDummy;
	file >> out_param.m_vecS(X);
	out_param.m_vecS(X) = 1.0 / out_param.m_vecS(X);

	while (strDummy.compare("av")){
		file >> strDummy;
	}
	file >> strDummy;
	file >> out_param.m_vecS(Y);
	out_param.m_vecS(Y) = 1.0 / out_param.m_vecS(Y);

	while (strDummy.compare("dimx")){
		file >> strDummy;
	}
	file >> strDummy;
	file >> out_param.m_vecDim(X);

	while (strDummy.compare("dimy")){
		file >> strDummy;
	}
	file >> strDummy;
	file >> out_param.m_vecDim(Y);

	while (strDummy.compare("Focale:")){
		file >> strDummy;
	}
	file >> out_param.m_dZprime;

	while (strDummy.compare("Rotation:")){
		file >> strDummy;
	}
	file >> cDummy;
	file >> cDummy;
	file >> out_param.m_matR(0, 0);
	file >> cDummy;
	file >> out_param.m_matR(0, 1);
	file >> cDummy;
	file >> out_param.m_matR(0, 2);
	file >> strDummy;
	file >> cDummy;
	file >> out_param.m_matR(1, 0);
	file >> cDummy;
	file >> out_param.m_matR(1, 1);
	file >> cDummy;
	file >> out_param.m_matR(1, 2);
	file >> strDummy;
	file >> cDummy;
	file >> out_param.m_matR(2, 0);
	file >> cDummy;
	file >> out_param.m_matR(2, 1);
	file >> cDummy;
	file >> out_param.m_matR(2, 2);

	while (strDummy.compare("Translation:")){
		file >> strDummy;
	}
	file >> cDummy;
	file >> out_param.m_vecT(0);
	file >> cDummy;
	file >> out_param.m_vecT(1);
	file >> cDummy;
	file >> out_param.m_vecT(2);

	file.close();

	return true;

}

/*******************************************************************************
bool enregistrerImage(vector<fichierIm>& i_vImages)

Entree:		i_vImages : vecteur conteneur dans lequel les images sont contenues
Sortie:		aucune
Retour:		aucun

L'usager choisi l'image devant �tre sauvegard�e parmi celles �tant d�j� en m�moire
*******************************************************************************/

void enregistrerImage(vector<fichierIm>& io_vImgs)
{
	cout << endl << "-----------> Sauvegarde d'une image <-----------" << endl;

	unsigned int unIndIm = choixImages(io_vImgs);

	if (!enregistrerImage(io_vImgs[unIndIm]))
		cout << endl << "PROBLEME : sauvegarde" << endl;
}

/***********************************************************************************
void listerImages((const vector<fichierIm>& i_vImages))

Entr�e: i_vImages : vecteur conteneur dans lequel les images sont contenues
Sortie: aucune
Retour: aucun

Fonction s'occupant d'afficher la liste des images qui sont charg�es en m�moire.
***********************************************************************************/

void listerImages(const vector<fichierIm>& i_vImgs)
{
	unsigned int unCompteur;

	cout << endl << "-----------> Images en memoire <-----------" << endl << endl;

	for (unCompteur = 0; unCompteur<i_vImgs.size(); unCompteur++)
		cout << unCompteur + 1 << " - " << i_vImgs[unCompteur].nom() << endl;

}

/**********************************************************************************
unsigned int choixImages(const vector<fichierIm>& i_vImages)

Entree:		i_vImages : vecteur conteneur dans lequel les images sont contenues
Sortie:		aucune
Retour:		entier : indice de position d'une image

Fonction effectuant l'affichage de toutes les images ayant d�j� �t� charg�es en
m�moire. L'usager fait le choix de l'image. La fonction va s'assurer que
l'usager fournit un choix correct, sans quoi la fonction va redemander � l'usager
de faire un choix tant et aussi longtemps que celui-ci donne un choix non valide.
Le retour de la fonction est l'indice de position de l'image.
***********************************************************************************/

int choixImages(const vector<fichierIm>& i_vImgs)
{
	string cChoix;
	unsigned int unNumImage;
	bool bNotVal(true);

	if (i_vImgs.size() == 0){
		cout << endl << "Il n'y a aucune image en memoire, veuillez en entrez avant !" << endl;
		return -1;
	}

	do {
		listerImages(i_vImgs);

		cout << endl << "Votre choix => ";
		cin >> cChoix;

		unNumImage = atoi(cChoix.data()) - 1;

		bNotVal = (unNumImage < 0 || unNumImage > i_vImgs.size() - 1);

		if (bNotVal)
			cout << endl << "Choix non valide" << endl;

	} while (bNotVal);

	cout << endl << "Image choisie : " << i_vImgs[unNumImage].nom() << endl;

	return unNumImage;
}

/***********************************************************************************
void lisser(const vector<fichierIm>& i_vImages)

Entr�e: i_vImages : vecteur conteneur dans lequel les images sont contenues
Sortie: aucune
Retour: aucun

Fonction permettant de r�aliser un lissage Gaussien sur une image en m�moire
choisie par l'usager. Le sigma, ainsi que le domaine dans lequel le lissage doit
�tre effectu� sont demand�s � l'usager.
***********************************************************************************/

void lisser(const vector<fichierIm>& i_vImgs)
{
	unsigned int unIndIm;
	double dSigma;
	char cChoix;
	int nType;
	string strNomFichier;
	fichierIm imgRes;

	cout << endl << "-----------> Lissage Gaussien <-----------" << endl << endl;

	unIndIm = choixImages(i_vImgs);

	cout << endl << "Echelle de lissage => ";
	cin >> dSigma;

	do
	{
		cout << "Methode de lissage :" << endl
			<< "S - Domaine spatial" << endl
			<< "F - Domaine frequentiel" << endl;
		cin >> cChoix;
		cChoix = toupper(cChoix);

		switch (cChoix) {
		case 'S':
			nType = CDIRECTE;
			break;
		case 'F':
			nType = CFOURIER;
			break;
		default:
			cout << endl << "Choix non valide" << endl;
			cChoix = NONVALIDE;
		}

	} while (cChoix == NONVALIDE);

	if (lissageGaussien(imgRes.image(), i_vImgs[unIndIm].image(), dSigma, nType)){
		cout << "Enregistrement du resultat du lissage" << endl;
		if (!enregistrerImage(imgRes))
			cout << endl << "PROBLEME : lissage - enregistrement de l'image r�sultat" << endl;
	}
	else
		cout << endl << "PROBLEME : lissage" << endl;
}

/**************************************************************************************
void detecterJonctions(const vector<fichierIm>& io_vImgs)

Entr�e: i_vImages : vecteur conteneur dans lequel les images sont contenues
Sortie: aucune
Retour: aucun

La fonction permet � l'usager de faire la d�tection des jonctions sur une image qu'il
a choisi en m�moire. Pour effectuer celle-ci l'usager doit fournir le sigma avec lequel
il desire faire la d�tection des contours
**************************************************************************************/

void detecterJonctions(vector<fichierIm>& io_vImgs)
{
	unsigned int uiIndIm;
	double dSigmaLow, dSigmaHigh, dThreshLow, dThreshHigh;
	string strTmp;
	fichierIm imgRes;
	char tmp[50];

	cout << endl << "-----------> Detection de contours <-----------" << endl << endl;

	uiIndIm = choixImages(io_vImgs);

	cout << endl << "Echelle basse de detection => ";
	cin >> dSigmaLow;

	cout << endl << "Echelle haute de detection => ";
	cin >> dSigmaHigh;

	cout << endl << "Seuil pour la detection a basse echelle => ";
	cin >> dThreshLow;

	cout << endl << "Seuil pour la detection a haute echelle => ";
	cin >> dThreshHigh;

	strTmp = removeExt(io_vImgs[uiIndIm].nom());

	if (io_vImgs.size() <= MAX_IMAGES - 1){

		if (TabonneCornerDetect(imgRes.image(), io_vImgs[uiIndIm].image(), dSigmaLow, dSigmaHigh, dThreshLow, dThreshHigh)){
			sprintf(tmp, "l%.2fh%.2f", dSigmaLow, dSigmaHigh);
			imgRes.nom() = strTmp + "Jct" + tmp + ".ppm";
			io_vImgs.push_back(imgRes);
		}
		else
			cout << endl << "PROBLEME : jonctions" << endl;
	}
}


/**************************************************************************************
void seuiller(const vector<fichierIm>& i_vImages)

Entr�e: i_vImages : vecteur conteneur dans lequel les images sont contenues
Sortie: aucune
Retour: aucun

La fonction permet � l'usager de faire le seuillage d'une image qu'il
a choisi en m�moire. Pour effectuer celle-ci l'usager doit fournir le seuil.
**************************************************************************************/

void seuiller(const vector<fichierIm>& i_vImgs)
{
	unsigned int uiIndIm;
	double dSeuil;
	string strNomFichier;
	fichierIm imgRes;

	cout << endl << "-----------> Seuillage <-----------" << endl << endl;

	uiIndIm = choixImages(i_vImgs);

	cout << endl << "Seuil = >";
	cin >> dSeuil;

	if (seuillage(imgRes.image(), i_vImgs[uiIndIm].image(), dSeuil)){
		cout << "Enregistrement du resultat du seuillage" << endl;
		if (!enregistrerImage(imgRes))
			cout << endl << "PROBLEME : seuillage - enregistrement de l'image r�sultat" << endl;
	}
	else
		cout << endl << "PROBLEME : seuillage" << endl;
}

string removeExt(
	string i_strBaseFileName
	)
{
	int pos;
	string tmp;

	pos = i_strBaseFileName.find(".pgm");

	if (pos == string::npos)
		pos = i_strBaseFileName.find(".ppm");

	if (pos == string::npos)
		pos = i_strBaseFileName.find(".pnm");

	if (pos == string::npos)
		pos = i_strBaseFileName.find(".png");

	if (pos != string::npos)
		tmp = i_strBaseFileName.erase(pos, 4);
	else
		tmp = i_strBaseFileName;

	return tmp;

}

/**********************************************************************************
bool lirePtsScene(vector<point3d>& o_vPts)

Entr�e: Aucun
Sortie: Pts de la sc�ne
Retour: boul�en, vrai si le chargement s'effectue bien

Cette fonction demande � l'usager d'entrer le nom du fichier des param�tres et lit
les points de la sc�ne sous forme de vecteur de points 3D
**********************************************************************************/
bool lirePtsScene(vector<point3d>& o_vPts){
	string strNomFichier;

	cout << endl << "-----------> Ouverture d'un fichier de points <-----------" << endl
		<< endl << "Nom du fichier (avec extension) => ";
	cin >> strNomFichier;

	return lirePtsSceneFile(o_vPts, strNomFichier);

}

/**********************************************************************************
bool lireCalib(calibParam& param)

Entr�e: Aucun
Sortie: Param�tres de calibration
Retour: boul�en, vrai si le chargement s'effectue bien

Cette fonction demande � l'usager d'entrer le nom du fichier des param�tres et lit les param�tres de calibrage appropri�s
**********************************************************************************/
bool lireCalib(calibParam& param){

	string strNomFichier;

	cout << endl << "-----------> Ouverture d'un fichier de calibrage <-----------" << endl
		<< endl << "Nom du fichier (avec extension) => ";
	cin >> strNomFichier;

	return lireCalibFile(param, strNomFichier);

}


/***********************************************************************************
void calibrerCamera(const vector<fichierIm>& i_vImgs)

Entr�e: i_vImages : vecteur conteneur dans lequel les images sont contenues
Sortie: aucune
Retour: aucun

Fonction permettant de r�aliser un calibrage d'une cam�ra, � partir d'une image et
des coordonn�es d'une liste de points 3D
***********************************************************************************/

bool calibrerCamera(const vector<fichierIm>& i_vImgs){
	unsigned int uiIndIm;
	vector<point3d> vPts;
	calibParam param;

	uiIndIm = choixImages(i_vImgs);

	if (!lirePtsScene(vPts))
		return false;

	if (!calibrerCamera(param, vPts, i_vImgs[uiIndIm].image()))
		return false;

	return ecrireCalibFile(param, "camera.calib");
}


bool rectification(const vector<fichierIm>& i_vImgs){
	unsigned int uiIndImG, uiIndImD;
	fichierIm imageGaucheRect, imageDroiteRect; // image des profondeurs
	vector<point3d> vPts;
	calibParam paramG, paramD;

	cout << endl << "%%%%%%%%%%%%%%%% CHOIX DE L'IMAGE DE GAUCHE %%%%%%%%%%%%%%%%" << endl;
	uiIndImG = choixImages(i_vImgs);
	if (uiIndImG == -1) return false;

	cout << endl << "%%%%%%%%%%%%%%%% CHOIX DE L'IMAGE DE DROITE %%%%%%%%%%%%%%%%" << endl;
	uiIndImD = choixImages(i_vImgs);
	if (uiIndImD == -1) return false;

	cout << endl << "%%%%%%%%%%%%%%%% CAMERA GAUCHE %%%%%%%%%%%%%%%%" << endl;
	lireCalib(paramG);

	cout << endl << "%%%%%%%%%%%%%%%% CAMERA DROITE %%%%%%%%%%%%%%%%" << endl;
	lireCalib(paramD);

	if (!rectifier(imageGaucheRect.image(), imageDroiteRect.image(), i_vImgs[uiIndImG].image(), i_vImgs[uiIndImD].image(), paramG, paramD))
		return false;

	if (!enregistrerImage(imageGaucheRect))
		cout << endl << "PROBLEME : rectificationgauche - enregistrement de l'image r�sultat" << endl;

	if (!enregistrerImage(imageDroiteRect))
		cout << endl << "PROBLEME : rectificationdroite - enregistrement de l'image r�sultat" << endl;

	return true;

}

/**********************************************************************************
void ouvrirImage(vector<fichierIm>& io_vImages)

Entr�e:		io_vImages : vecteur conteneur dans lequel les images sont contenues
Sortie:		io_vImages : vecteur conteneur dans lequel les images sont contenues
Retour:		aucun

Cette fonction re�oit en param�tre io_vImages, un vecteur tel que defini dans les STL.
Celui-ci, peut-etre vide ou non. Apr�s v�rification que le vecteur n'est pas d�j� plein,
la fonction charge l'image en m�moire. Si le chargement s'effectue correctement, l'image est
ajout�e  dans le vecteur
**********************************************************************************/
void ouvrirImage(vector<fichierIm>& io_vImgs)
{
	fichierIm imResultat;

	if (io_vImgs.size() < MAX_IMAGES)	{
		if (ouvrirImage(imResultat, "Images\\gauche.ppm"))
			io_vImgs.push_back(imResultat);
		else
			cout << "PROBLEME : ouverture" << endl;
		if (ouvrirImage(imResultat, "Images\\droite.ppm"))
			io_vImgs.push_back(imResultat);
		else
			cout << "PROBLEME : ouverture" << endl;
	}
	else
		cout << "PROBLEME : la capacite maximale de " << MAX_IMAGES << " images, est atteinte" << endl;
}

bool stereovision(vector<fichierIm>& i_vImgs){
	unsigned int uiIndImG, uiIndImD;
	double dProfMin, dProfMax;
	fichierIm imageProf; // image des profondeurs
	vector<point3d> vPts;
	calibParam paramG, paramD;

	ouvrirImage(i_vImgs);

	cout << endl << "%%%%%%%%%%%%%%%% CHOIX DE L'IMAGE DE GAUCHE %%%%%%%%%%%%%%%%" << endl;
	//uiIndImG = choixImages(i_vImgs);
	//if (uiIndImG == -1) return false;

	cout << endl << "%%%%%%%%%%%%%%%% CHOIX DE L'IMAGE DE DROITE %%%%%%%%%%%%%%%%" << endl;
	//uiIndImD = choixImages(i_vImgs);
	//if (uiIndImD == -1) return false;

	cout << endl << "%%%%%%%%%%%%%%%% CAMERA GAUCHE %%%%%%%%%%%%%%%%" << endl;
	//lireCalib(paramG);

	cout << endl << "%%%%%%%%%%%%%%%% CAMERA DROITE %%%%%%%%%%%%%%%%" << endl;
	//lireCalib(paramD);

	if (!calculerProfondeur(imageProf.image(), dProfMin, dProfMax, i_vImgs[0].image(), i_vImgs[1].image()/*, paramG, paramD*/))
		return false;

	cout << "Profondeur minimale (pixels fonc�s): " << dProfMin << endl;
	cout << "Profondeur maximale (pixels blancs): " << dProfMax << endl;

	if (!enregistrerImage(imageProf))
		cout << endl << "PROBLEME : stereo - enregistrement de l'image r�sultat" << endl;

	return true;

}


int main()
{
	bool bContinue(true);
	//	unsigned int uiNbImages(0);
	char cChoixUsager;
	vector<fichierIm> v_imImages;
	vector<point3d>	  vPts;

	cout << "Bienvenue dans le logiciel d'analyse d'image" << endl;
	//	ouvrirImage(v_imImages);

	do
	{
		cout << endl
			<< "****************************************" << endl << endl
			<< "Operations : " << endl
			<< "\t O - Ouverture d'une image" << endl
			<< "\t S - Sauvegarde d'une image" << endl
			<< "\t L - Lister les images en memoire" << endl
			<< "\t P - Lire un fichier de points de la scene" << endl
			<< "\t C - Effectuer le calibrage" << endl
			<< "\t R - Rectifier une paire stereo" << endl
			<< "\t V - Calculer la profondeur a partir d'une paire stereo" << endl
			<< "\t D - Detection des jonctions" << endl
			<< "\t T - Seuillage" << endl
			<< "\t Q - Quitter" << endl
			<< endl << "Entrez votre choix => ";

		cin >> cChoixUsager;
		cChoixUsager = toupper(cChoixUsager);

		switch (cChoixUsager)
		{
		case 'O':
			ouvrirImage(v_imImages);
			break;

		case 'S':
			enregistrerImage(v_imImages);
			break;

		case 'L':
			listerImages(v_imImages);
			break;

		case 'C':
			calibrerCamera(v_imImages);
			break;

		case 'D':
			detecterJonctions(v_imImages);
			break;

		case 'T':
			seuiller(v_imImages);
			break;

		case 'P':
			lirePtsScene(vPts);
			break;

		case 'R':
			rectification(v_imImages);
			break;

		case 'V':
			stereovision(v_imImages);
			break;

		case 'Q':
			bContinue = !confirmerArret();
			break;

		default:	cout << endl << "Choix non valide" << endl;
		}

	} while (bContinue);

	return 0;
}
