/*******************************************************

mettre vos noms et matricules ici

********************************************************/

#include <iostream>
#include <math.h>
#include "fonctions.h"
#include "TabonneCornerDetect.h"
#include "fichierIm.h"

using namespace std;

bool calibrerStereo(math::matrix<double>& o_R, math::matrix<double>& o_T, const calibParam& i_calibGauche, const calibParam& i_calibDroite){
	
	// A MODIFIER : Ceci est du code de test
	cout<<"le calibrage du systeme stereo s'execute..."<<endl;
	
	return true;
}

bool calibrerCamera(calibParam& o_param, const vector<point3d>& i_vPts, const Image& i_img){
	// A MODIFIER : Ceci est du code de test
	cout<<"le calibrage s'execute..."<<endl;

	return true;
}

bool calculerProfondeur(Image& o_imgProf, double& o_dProfMin, double& o_dProfMax, const Image& i_imgGauche, const Image& i_imgDroite 
						/*const calibParam& i_calibGauche*//*, const calibParam& i_calibDroite*/){

	carteProfondeur carte_p;
	Image gauche = i_imgGauche;
	Image droite = i_imgDroite;

	o_imgProf = carte_p.getCarteProfondeur(gauche, droite);
	cout<<"la stereo s'execute..."<<endl;

	return true;
}

bool rectifier(Image& o_imgGRect, Image& o_imgDRect, const Image& i_imgGauche, const Image& i_imgDroite, const calibParam& i_calibGauche, const calibParam& i_calibDroite){

	// A MODIFIER : Ceci est du code de test
	// La calibration du syst�me st�r�o � partir des cam�ras individuelles doit se faire � partir de cette fonction
	
	cout<<"la rectification s'execute..."<<endl;

	return true;
}

bool changementDynamique(Image& o_imgRes, const Image& i_imgOrig, int i_nType)
{
	if (i_nType == DLIN )
		cout<<"changement lineaire"<<endl;
	else
		cout<<"changement logarithmique"<<endl;

	cout<<"le changement de dynamique s'execute..."<<endl;
	
	double dMax, dMin, a, b;
	int i;

	int 	nWidth(i_imgOrig.GetWidth()),
			nHeight(i_imgOrig.GetHeight()),
			nBands(i_imgOrig.GetNumBands());

	if (nWidth*nHeight*nBands == 0){
		cout<<"Image en ent�e vide"<<endl;
		return false;
	}

	if(!o_imgRes.Resize(nWidth, nHeight, nBands))
		return false;

	for (int k=0; k<nBands; k++){
		PIXTYPE*  pSrc = i_imgOrig.GetBuffer(k);
		PIXTYPE*  pDst = o_imgRes.GetBuffer(k);

		dMax = dMin = pSrc[0];

		for(i = 1; i < nWidth*nHeight; i++) {
			if ( pSrc[i] < dMin) 
				dMin = pSrc[i];
			else if ( pSrc[i] > dMax)
				dMax = pSrc[i];
		}       

		if (i_nType == DLIN){
			a = 255 / (dMax - dMin);
			b = -dMin * a;

			for(i = 0; i < nWidth*nHeight; i++){
				pDst[i] = (PIXTYPE) (a * pSrc[i] + b);
			}
		}
			
		else if (i_nType == DLOG) {
			a = 255 / log(1 + (dMax - dMin));

			for(i = 0; i < nWidth*nHeight; i++){
				pDst[i] = (PIXTYPE) (a * log(1 + (pSrc[i] - dMin)));
			}
		
		}
	}

	return true;
}


bool convolution(Image& o_imgRes, const Image& i_imgOrig, const Image& i_imgFiltre, 
	const int& i_nType)
{
	// A MODIFIER : Ceci est du code de test
	if (i_nType == CDIRECTE )
		cout<<"convolution directe"<<endl;
	else
		cout<<"convolution par Fourier"<<endl;

	cout<<"la convolution s'execute..."<<endl;

	return true;
}

bool convolution1D(Image& o_imgRes, const Image& i_imgOrig, const Bande& i_imgFiltreX, const Bande& i_imgFiltreY)
{
	cout<<"la convolution 1D s'execute..."<<endl;

	register int i, j, l, high(i_imgOrig.GetHeight()), larg(i_imgOrig.GetWidth());

	PIXTYPE nv, *d, *d1, *d2, *temp;
	int tm_g(i_imgFiltreY.GetWidth()),
		tm_f(i_imgFiltreX.GetWidth());
	
	if (!o_imgRes.Resize(larg, high, i_imgOrig.GetNumBands()))
		return false;
			
	o_imgRes.InitWith(0);
	Bande tmp(larg, high);

	for (int b=0; b<i_imgOrig.GetNumBands(); b++){
			
		tmp.InitWith(0);
		
		// initialisation des structures 
		d = d1 = tmp.GetBuffer();
		
		// convolution en x  de la fct de detection 
		// le masque in_mskx est passe en premier dans le sens des j
		for (i = 0; i < high; i++){
			for (j = 0; j < larg; j++) {
				for (l = -(tm_f/2); l <= (tm_f/2); l++){
					if ((j+l) < 0) 
						nv = i_imgOrig[b](i*larg);
					else if ((j+l) >= larg) 
						nv = i_imgOrig[b](((i+1)*larg)-1);
					else 
						nv = i_imgOrig[b]((i*larg)+j+l);
					
					*d = (nv * i_imgFiltreX((tm_f/2)-l)) + *d;
				}
				
				d++;
			}
		}
		
		d = d1;	
		d2 = o_imgRes.GetBuffer(b);
		
		//  convolution en y avec la fct de projection 
		// le masque in_msky est passe dans le sens des i=Y
		for (j = 0; j < larg ; j++){
			for (i = 0; i < high; i++) {
				for (l = -(tm_g/2); l <= (tm_g/2); l++){
					if ((i+l) < 0) 
						nv = d[j];
					else if ((i+l) >= high) 
						nv = d[((high-1)*larg)+j];
					else 
						nv = d[((i+l)*larg)+j];
					temp = (d2+(i*larg)+j);
					*temp = (nv * i_imgFiltreY((tm_g/2)-l)) + *temp;
				}
			}
		}
	}
		
	return true; 
}


bool lissageGaussien(Image& o_imgRes, const Image& i_imgOrig, const double& i_dSigma, const int& i_nType)
{
	// A MODIFIER : Ceci est du code de test
	if (i_nType == CDIRECTE )
		cout<<"lissage dans le domaine spatial"<<endl;
	else
		cout<<"lissage dans le domaine frequentiel"<<endl;

	cout<<"le lissage s'execute..."<<endl;

	return true;
}


bool detectionContours(Image& o_imgRes, const Image& i_imgOrig, const double& i_dSigma)
{
	// A MODIFIER : Ceci est du code de test
	cout<<"la detection de contours s'execute..."<<endl;

	return true;
}


bool seuillage(Image& o_imgRes, const Image& i_imgOrig, const double& i_dSeuil)
{
	cout<<"le seuillage s'execute..."<<endl;

	int 	nWidth(i_imgOrig.GetWidth()),
			nHeight(i_imgOrig.GetHeight()),
			nBands(i_imgOrig.GetNumBands());

	if(!o_imgRes.Resize(nWidth, nHeight, nBands))
		return false;
			
	for (int b=0; b<nBands; b++){
		PIXTYPE* pDst = o_imgRes.GetBuffer(b);
		PIXTYPE* pSrc = i_imgOrig.GetBuffer(b);
 
		for (int i = 0; i < nWidth*nHeight; i++){
			if (pSrc[i] > i_dSeuil)  
				pDst[i] = 255;
			else
				pDst[i] = 0;
		}
	}
			
	return true;
}


bool fourier(Image& o_imgReel, Image& o_imgImag, const Image& i_imgOrig)
{
	// A MODIFIER : Ceci est du code de test
	cout<<"la transformee de Fourier s'execute..."<<endl;

	return true;
}

bool spectreAmplitude(Image& o_imgRes, const Image& i_imgReel, const Image& i_imgImag)
{
	// A MODIFIER : Ceci est du code de test
	cout<<"le calcul du spectre d'amplitude s'execute..."<<endl;

	return true;
}

bool spectrePhase(Image& o_imgRes, const Image& i_imgReel, const Image& i_imgImag)
{
	// A MODIFIER : Ceci est du code de test
	cout<<"le calcul du spectre de phase s'execute..."<<endl;

	return true;
}

bool fourierInverse(Image& o_imgRes, const Image& i_imgReel, const Image& i_imgImag)
{
	// A MODIFIER : Ceci est du code de test
	cout<<"la transformee de Fourier inverse s'execute..."<<endl;

	return true;
}

inline double fctErf(double in_dOperande, double in_dScale)
{
	return erf(in_dOperande/(sqrt(2.) * in_dScale));
}

inline double fctExp(double in_dOperande2, double in_dScale2)
{
	return exp(-(in_dOperande2)/(2.*in_dScale2));
}

int gaussMask1D(Bande& out_msk,	double in_dScale)
{
	int 		nMaskDim;
	double		b1, b2;

	// calcul de la taille du masque 
	double t = 6 * in_dScale;
	nMaskDim = (int) (t + 0.5);
	
	if (nMaskDim % 2 == 0) {
		if (t >= ((double) nMaskDim))
			nMaskDim += 1;
		else 
			nMaskDim -= 1;
	}
		
	out_msk.Resize(nMaskDim, 1);
			  
	// calcul des fonctions discretes de projection
	PIXTYPE* vct = out_msk.GetBuffer();	//  vecteur temporaire

	int nHalfDim = nMaskDim / 2;
	
	for (int i = 0; i < nHalfDim; i++) {
		b1 = 0.5 + (double) i;
		b2 = - 0.5 + (double) i;
		vct[i+nHalfDim] = (PIXTYPE) (0.5 * (fctErf(b1, in_dScale)-fctErf(b2, in_dScale)));
		vct[nHalfDim-i] = vct[i+nHalfDim];			//symmetrie
	}

	b2 = ((double) nHalfDim) - 0.5;
	vct[2 * nHalfDim] = (PIXTYPE) (0.5 * (1.-fctErf(b2, in_dScale)));
	vct[0] = vct[2 * nHalfDim];						//symmetrie
	
	return(nHalfDim);
}


int gaussDerivMask1D(Bande& out_msk, double in_dScale)
{
	int			i,
				nMaskDim;

	// calcul de la taille du masque 
	double t =  7 * in_dScale;
	nMaskDim = (int) (t + 0.5);
	
	if (nMaskDim % 2 == 0) {
		if (t >= ((double) nMaskDim))
			nMaskDim += 1;
		else 
			nMaskDim -= 1;
	}
		
	out_msk.Resize(nMaskDim, 1);
			  
	// calcul des fonctions discretes de detection et de projection
	// la partie vctd est la partie derivee alors que vct est la projection
	PIXTYPE* vctd = out_msk.GetBuffer();		
	
	int nHalfDim = nMaskDim / 2;
	
	double	dScale2 = in_dScale * in_dScale,
			kd = 1 / ( in_dScale * sqrt(2.0*M_PI)),
			b1, b2;

	for (i = 0; i < nHalfDim; i++) {
		b1 = 0.5f + (double) i;
		b2 = - 0.5f + (double) i;
		vctd[i+nHalfDim] = (PIXTYPE) (kd*(fctExp(b1*b1, dScale2) - fctExp(b2*b2, dScale2)));
		vctd[nHalfDim-i] = - vctd[i+nHalfDim];
	}
	
	b2 = (double) nHalfDim - 0.5;
	vctd[2*nHalfDim] = (PIXTYPE) (- kd * fctExp(b2*b2, dScale2));
	vctd[0] = - vctd[2 * nHalfDim];

	return nHalfDim;   
}

int gaussSecondDerivMask1D(Bande& out_msk, double in_dScale)
{	
	int			i, 
				nMaskDim;

	// calcul de la taille du masque
	double t = 8 * in_dScale;
	nMaskDim = (int) (t + 0.5);
        
	if (nMaskDim % 2 == 0) {
		if (t >= ((double) nMaskDim))
			nMaskDim += 1;
		else 
			nMaskDim -= 1;
	}
                
	out_msk.Resize(nMaskDim, 1);
			  
	// calcul des fonctions discretes de detection et de projection
	// la partie vctd est la partie derivee alors que vct est la projection
	PIXTYPE* vctdd = out_msk.GetBuffer();             //  vecteur temporaire
 
	int nHalfDim = nMaskDim / 2;
        
	double	dScale2 = in_dScale * in_dScale,
			kdd = -1.0 / (in_dScale * dScale2 * sqrt(2.0 * M_PI)),
			b1, b2;

	for (i = 0; i < nHalfDim; i++) {
		b1 = 0.5 + (double) i;
		b2 = - 0.5 + (double) i;
		vctdd[i+nHalfDim] = (PIXTYPE) (kdd*(b1*fctExp(b1*b1, dScale2) 
			- b2*fctExp(b2*b2, dScale2)));
		vctdd[nHalfDim-i] = vctdd[i+nHalfDim];
	}
        
	b2 = ((double) nHalfDim) - 0.5;
	
	vctdd[2*nHalfDim] =  (PIXTYPE) (-kdd* b2 * fctExp(b2*b2, dScale2));
	vctdd[0] = vctdd[2*nHalfDim];

	return(nHalfDim);   
}

bool InfToZero(double in_dVal){
	return (!IsZero(in_dVal) && (in_dVal < 0.0));
}

PIXTYPE IsZeroCrossing(
	const Bande& i_band, 
	int i, 
	int j
)
{
	// dans la direction horizontale
	if ((!IsZero(i_band(i,j+1)) && !IsZero(i_band(i,j-1)) 
			&& InfToZero(i_band(i,j+1)*i_band(i,j-1)))
		// dans la direction verticale
			|| (!IsZero(i_band(i+1,j)) && !IsZero(i_band(i-1,j)) 
				&& InfToZero(i_band(i+1,j)*i_band(i-1,j)))
		// dans la direction de la premiere diagonale
			|| (!IsZero(i_band(i+1,j+1)) && !IsZero(i_band(i-1,j-1)) 
				&& InfToZero(i_band(i+1,j+1)*i_band(i-1,j-1)))
		// dans la direction de la deuxieme diagonale
			|| (!IsZero(i_band(i+1,j-1)) && !IsZero(i_band(i-1,j+1))
				&& InfToZero(i_band(i+1,j-1)*i_band(i-1,j+1))))
		return((PIXTYPE) 255);
	else 
		return((PIXTYPE) 0);
}

bool ZeroCrossings(Image& o_imgZC, const Image&	i_img)
{
	int	nWidth(i_img.GetWidth()),
		nHeight(i_img.GetHeight()),
		nBands(i_img.GetNumBands());

	if(!o_imgZC.Resize(nWidth, nHeight, nBands))
		return false;
	
  	o_imgZC.InitWith(0);

	for (int i = 1; i < nWidth-1; i++){
		for (int j = 1; j < nHeight-1; j++){
			// recherche des passages par zero dans 4 directions
			// si le contour n'est pas marque 
			if (o_imgZC[0](i,j) == 0){
				if (IsZero(i_img[0](i,j))) 
					o_imgZC[0](i,j) = IsZeroCrossing(i_img[0], i, j);
				else {
					// dans la direction horizontale
					if (InfToZero(i_img[0](i,j)*i_img[0](i,j+1))){
						if (fabs((double) i_img[0](i,j))< fabs((double) i_img[0](i,j+1)))
							o_imgZC[0](i,j) = (PIXTYPE) 255;
						else 
							o_imgZC[0](i,j+1) = (PIXTYPE) 255;
					}

					// dans la premiere direction diagonale
					if (InfToZero(i_img[0](i,j)*i_img[0](i+1,j+1))) {
						if (fabs((double) i_img[0](i,j)) < fabs((double) i_img[0](i+1,j+1)))
							o_imgZC[0](i,j) = (PIXTYPE) 255;
						else 
							o_imgZC[0](i+1,j+1) = (PIXTYPE) 255;
					}

					// dans la direction verticale
					if (InfToZero(i_img[0](i,j)*i_img[0](i+1,j))) {
						if (fabs((double) i_img[0](i,j)) < fabs((double) i_img[0](i+1,j)))
							o_imgZC[0](i,j) = (PIXTYPE) 255;
						else 
							o_imgZC[0](i+1,j) = (PIXTYPE) 255;
					}

					// dans la deuxieme direction diagonale
					if (InfToZero(i_img[0](i,j)*i_img[0](i+1,j-1))) {
						if (fabs((double) i_img[0](i,j)) < fabs((double) i_img[0](i+1,j-1)))
							o_imgZC[0](i,j) = (PIXTYPE) 255;
						else 
							o_imgZC[0](i+1,j-1) = (PIXTYPE) 255;
					}
				}
			}			
		}
	}

	return true;
}
