#ifndef FONCTIONS_H
#define FONCTIONS_H

#include <vector>
#include "image.h"
#include "points.h"
#include "matrix.h"
#include "carteProfondeur.h"

#define CDIRECTE   1
#define CFOURIER   2
#define DLIN	   1
#define DLOG	   2

using namespace std;

#define vectD vect<double>

struct calibParam{
		// Constructeurs
	calibParam(): m_dZprime(1), m_vecS(2){
		m_vecO.Null(2);
		
		m_vecS(X) = 1;
		m_vecS(Y) = 1;
		
		m_vecDim.Null(2);
		
		m_vecT.Null(3);

		m_matR.Unit(3);
	};

	// INTRINSEQUES
	// centre de l'image en pixels
	vectD m_vecO;

	// facteur d'�chelle (�chantillonage) en unit� d'espace
	vectD m_vecS;

	// dimensions de l'image en pixels
	vect<int> m_vecDim;

	// dist. du plan image en unit� d'espace
	double m_dZprime; 

	// EXTRINSEQUES
	// translation
	vectD m_vecT;
	
	matrixD m_matR;

};

/******************************************************************************
bool calculerProfondeur(Image& o_imgProf, const Image& i_imgGauche, const Image& i_imgDroite, 
				   const paramCalib& i_calibGauche, const paramCalib& i_calibDroite);

Entr�es:	i_imgGauche : image de gauche
			i_imgDroite : image de droite
			i_calibGauche : parametres de calibrage de la cam�ra de gauche
			i_calibDroite : parametres de calibrage de la cam�ra de droite
Sortie:		o_imgProf : image des profondeurs
			o_dProfMin : profondeur minimale dans l'image (en mm)
			o_dProfMax : profondeur maximale dans l'image (en mm)
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	La profondeur est calcul�e � partir de la paire st�r�oscopique
******************************************************************************/
bool calculerProfondeur(Image& o_imgProf, double& o_dProfMin, double& o_dProfMax, const Image& i_imgGauche, const Image& i_imgDroite
				   /*const calibParam& i_calibGauche, const calibParam& i_calibDroite*/);

/******************************************************************************
bool rectifier(Image& o_imgProf, const Image& i_imgGauche, const Image& i_imgDroite, 
				   const paramCalib& i_calibGauche, const paramCalib& i_calibDroite);

Entr�es:	i_imgGauche : image de gauche
			i_imgDroite : image de droite
			i_calibGauche : parametres de calibrage de la cam�ra de gauche
			i_calibDroite : parametres de calibrage de la cam�ra de droite
Sortie:		o_imgGRect : image gauche rectifi�e
			o_imgDRect : image droite rectifi�e
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	Les images de la paire st�r�oscopique sont rectifi�es
******************************************************************************/
bool rectifier(Image& o_imgGRect, Image& o_imgDRect, const Image& i_imgGauche, const Image& i_imgDroite, 
			   const calibParam& i_calibGauche, const calibParam& i_calibDroite);

/******************************************************************************
bool calibrerCamera(vector<point3d> i_vPts, const Image& i_img)

Entr�es:	i_img : image sur laquelle faire l'op�ration
			i_vPts : vecteur de points 3D
Sortie:		o_param : param�tres de la cam�ra
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	Un fichier de parametres de la camera sera cr�� et remplis
******************************************************************************/
bool calibrerCamera(calibParam& o_param, const vector<point3d>& i_vPts, const Image& i_img);

/******************************************************************************
 bool calibrerStereo(math::matrix<double>& o_R, math::matrix<double>& o_T, const calibParam& i_calibGauche, const calibParam& i_calibDroite)
 
 Entr�es:	i_calibGauche : parametres de calibrage de la cam�ra de gauche
			i_calibDroite : parametres de calibrage de la cam�ra de droite
 Sortie:	o_R : matrice de rotation entre les 2 cam�ras
			o_T : vecteur de translation entre les 2 centres optiques
 Retour:	bool�en, vrai si tout s'est bien d�roul�
 ******************************************************************************/
bool calibrerStereo(math::matrix<double>& o_R, math::matrix<double>& o_T, const calibParam& i_calibGauche, const calibParam& i_calibDroite);

/******************************************************************************
bool changementDynamique(Image& o_imgRes, const Image& i_imgOrig, int i_nType = DLIN)

Entr�es:	i_imgOrig : image sur laquelle faire l'op�ration
			i_nType : m�thode � utiliser	
				DLIN : lin�aire (d�faut)
				DLOG : logarithmique
Sortie:		o_imgRes : image r�sultante
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si le changement de dynamique s'effectue correctement, l'image o_imgRes est 
			�cras�e et contient le r�sultat
******************************************************************************/
bool changementDynamique(Image& o_imgRes, const Image& i_imgOrig, int i_nType = DLIN);


/******************************************************************************
bool convolution(Image& o_imgRes, const Image& i_imgOrig, const Image& i_imgFiltre, 
	const int& i_nType = CDIRECTE)

Entr�es:	i_imgOrig : image d'origine 
			i_imgFiltre : filtre
			i_nType : m�thode � utiliser
				CDIRECTE : convolution directe (d�faut)
				CFOURIER : convolution par la transform�e de Fourier
Sortie:		o_imgRes : image convolu�e
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si la convolution s'effectue correctement, l'image o_imgRes est 
			�cras�e et contient le r�sultat

******************************************************************************/
bool convolution(Image& o_imgRes, const Image& i_imgOrig, const Image& i_imgFiltre, 
	const int& i_nType = CDIRECTE);

/******************************************************************************
bool convolution1D(Image& o_imgRes, const Image& i_imgOrig, const Bande& i_imgFiltreX, const Bande& i_imgFiltreY)

Entr�es:	i_imgOrig : image d'origine 
			i_imgFiltreX : filtre 1D pour X
			i_imgFiltreY : filtre 1D pour Y
Sortie:		o_imgRes : image convolu�e
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si la convolution s'effectue correctement, l'image o_imgRes est 
			�cras�e et contient le r�sultat.  Cette fonction effectue une convolution 
			directe ligne par ligne et ensuite colonne par colonne (convient pour un 
			filtre 2D s�parable
******************************************************************************/
bool convolution1D(Image& o_imgRes, const Image& i_imgOrig, const Bande& i_imgFiltreX, const Bande& i_imgFiltreY);

/******************************************************************************
bool lissageGaussien(Image& o_imgRes, const Image& i_imgOrig, const double& i_dSigma = 1.0, 
	const int& i_nType = CDIRECTE)

Entr�es:	i_imgOrig : image originale
			i_dSigma : �chelle de lissage (d�faut = 1.0)
			i_nType : m�thode � utiliser
				CDIRECTE : lissage dans le domaine spatial (d�faut)
				CFOURIER : lissage dans le domaine fr�quentiel
Sortie:		o_imgRes : image liss�e
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si le lissage s'effectue correctement, l'image o_imgRes est 
			�cras�e et contient le r�sultat

REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
******************************************************************************/
bool lissageGaussien(Image& o_imgRes, const Image& i_imgOrig, const double& i_dSigma = 1.0, 
					 const int& i_nType = CDIRECTE);


/******************************************************************************
bool detectionContours(Image& o_imgRes, const Image& i_imgOrig, const double& i_dSigma = 1.0)

Entr�es:	i_imgOrig : image originale
			i_dSigma : �chelle de d�tection (d�faut = 1.0)
Sortie:		o_imgRes : images des contours d�tect�s
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si la d�tection s'effectue correctement, l'image o_imgRes est 
			�cras�e et contient le r�sultat

REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
******************************************************************************/
bool detectionContours(Image& o_imgRes, const Image& i_imgOrig, const double& i_dSigma = 1.0);


/******************************************************************************
bool seuillage(Image& o_imgRes, const Image& i_imgOrig, const double& i_dSeuil = 0)

Entr�es:	i_imgOrig : image originale
 			i_dSeuil : seuil d'�limination des contours (d�faut = 0 : aucune �limination)
Sortie:		o_imgRes : images des contours d�tect�s
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si le seuillage s'effectue correctement, l'image o_imgRes est 
			�cras�e et contient le r�sultat
******************************************************************************/
bool seuillage(Image& o_imgRes, const Image& i_imgOrig, const double& i_dSeuil = 0);


/******************************************************************************
bool fourier(Image& o_imgReel, Image& o_imgImag, const Image& i_imgOrig)

Entr�e:		i_imgOrig : image originale
Sorties:	o_imgReel : image des composantes r�elles de la TF
			o_imgImag : image des composantes imaginaires de la TF
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si la TF s'effectue correctement, les images o_imgReel et o_imgImag sont 
			�cras�es et contiennent le r�sultat

  REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
******************************************************************************/
bool fourier(Image& o_imgReel, Image& o_imgImag, const Image& i_imgOrig);


/******************************************************************************
bool spectreAmplitude(Image& o_imgRes, const Image& i_imgReel, const Image& i_imgImag)

Entr�es:	i_imgReel : images des composantes r�elles d'une image complexe
			i_imgImag : images des composantes imaginaires  d'une image complexe
Sortie:		o_imgRes : image r�sultante
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si le calcul du spectre d'amplitude s'effectue correctement, l'image o_imgRes est 
			�cras�e et contient le r�sultat

REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
******************************************************************************/
bool spectreAmplitude(Image& o_imgRes, const Image& i_imgReel, const Image& i_imgImag);

/******************************************************************************
bool fourierInverse(Image& o_imgRes, const Image& i_imgReel, const Image& i_imgImag)

Entr�es:	i_imgReel : images des composantes r�elles  d'une image complexe
			i_imgImag : images des composantes imaginaires  d'une image complexe
Sortie:		o_imgRes : image r�sultante
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si le calcul du spectre de phase s'effectue correctement, l'image o_imgRes est 
			�cras�e et contient le r�sultat

REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
******************************************************************************/
bool spectrePhase(Image& o_imgRes, const Image& i_imgReel, const Image& i_imgImag);

/******************************************************************************
bool fourierInverse(Image& o_imgRes, const Image& i_imgReel, const Image& i_imgImag)

Entr�es:	i_imgReel : images des composantes r�elles  d'une image complexe
			i_imgImag : images des composantes imaginaires  d'une image complexe
Sortie:		o_imgRes : image r�sultante
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	si la TF inverse s'effectue correctement, l'image o_imgRes est 
			�cras�e et contient le r�sultat

REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
REM : CETTE FONCTION N'EST PAS IMPLANT�E
******************************************************************************/
bool fourierInverse(Image& o_imgRes, const Image& i_imgReel, const Image& i_imgImag);

/******************************************************************************
bool gaussMask1D(Bande& out_msk,	double in_dScale)

Entr�es:	in_dScale : �chelle du masque
Sortie:		out_msk : masque 1D gaussien
Retour:		demie largeur du masque (effet de bord)
R�sultat:	cette fonction cr�� un masque gaussien 1D
******************************************************************************/
int gaussMask1D(Bande& out_msk,	double in_dScale);

/******************************************************************************
bool gaussDerivMask1D(Bande& out_msk,	double in_dScale)

Entr�es:	in_dScale : �chelle du masque
Sortie:		out_msk : masque 1D gaussien
Retour:		demie largeur du masque (effet de bord)
R�sultat:	cette fonction cr�� un masque de la premi�re d�riv�e de la gaussienne 1D
******************************************************************************/
int gaussDerivMask1D(Bande& out_msk, double in_dScale);

/******************************************************************************
bool gaussSecondDerivMask1D(Bande& out_msk,	double in_dScale)

Entr�es:	in_dScale : �chelle du masque
Sortie:		out_msk : masque 1D gaussien
Retour:		demie largeur du masque (effet de bord)
R�sultat:	cette fonction cr�� un masque de la seconde d�riv�e de la gaussienne 1D
******************************************************************************/
int gaussSecondDerivMask1D(Bande& out_msk, double in_dScale);

/******************************************************************************
bool ZeroCrossings(Image& out_edge, const Image& in_img)

Entr�es:	i_img : images en entr�e
Sortie:		o_imgZC : image des passages par z�ro
Retour:		bool�en, vrai si tout s'est bien d�roul�
R�sultat:	trouve les passages par z�ro dans une image
******************************************************************************/
bool ZeroCrossings(Image& o_imgZC, const Image& i_img);

#endif
