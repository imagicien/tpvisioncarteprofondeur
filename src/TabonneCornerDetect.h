// Détection de jonctions de type marche - Tabonne

#ifndef _CORNERTABONNE_H_
#define _CORNERTABONNE_H_

/*****************************************************************************
 Includes
*****************************************************************************/

#include "image.h"

/*****************************************************************************
 Defines
*****************************************************************************/

/*****************************************************************************
 Prototypes
*****************************************************************************/

// Prototypes de fonctions

bool TabonneCornerDetect(
	Image&			o_imgCorner,		// Image des jonctions
	const Image&	i_img,				// Image originale
	double			i_dLowScale, 		// Sigma basse echelle (autour de 1)
	double			i_dHighScale,		// Sigma haute echelle (autour de 2)
	PIXTYPE			i_LowThresh,		// Seuil basse echelle (autour de 2)
	PIXTYPE			i_HighThresh		// Seuil haute echelle (autour de 1)
);

#endif /* _CORNERTABONNE_H_ */

