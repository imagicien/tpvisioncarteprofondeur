/*****************************************************************************
 Includes
*****************************************************************************/

#include <vector>
#include "TabonneCornerDetect.h"
#include "fonctions.h"
#include "fichierIm.h"

#define 	NOTFOUND   -1

/*****************************************************************************
 Fonctions auxilliaires
*****************************************************************************/


static bool SearchExtremum(	Image& img_out,		
								const Image& lap , 
								const Image& hessian,
								PIXTYPE in_Thresh);

static bool MiseEnCorresp(	int& ibas, int& jbas, 
								const Bande& imagebas,
								const Bande& imagehaut, 
								const Bande& extre, 
								int i, int j, 
								vector<double>& in_theta);

static bool Tchebitcheff3x3(	float &icrt, 
								float &jcrt,
								const Bande& lap, 
								int i, 
								int j);

static bool FindZeroCross(		int& izc, int& jzc,
								const Bande& zc, 
								float ihautcrt, 
								float jhautcrt, 
								float ibascrt, 
								float jbascrt);

static bool IsExtremumMax(	const Bande& in_img,
							int x, int y);

static bool IsExtremumMin(	const Bande& in_img,
							int x, int y);

static bool IsExtremum(	const Image& in_img,
						int x, int y);
								
static void Normalize(vector<double> Theta){
	double norm = sqrt(sqrNorm(Theta[0], Theta[1]));
	Theta[0] = Theta[0]/norm;
	Theta[1] = Theta[1]/norm;
}
/*
void sauvegarde(Image Hessian, string nom){
	fichierIm fichTmp;
	Image tmp;

	changementDynamique(tmp, Hessian);
	fichTmp = fichierIm(tmp, nom);
	fichTmp.sauvegarde();
}
*/


/******************************************************************************
 *
 * Fonction  	:	TabonneCornerDetect
 * Creee le  	:	novembre 1998
 * Par       	:	Marie-Flavie Auclair-Fortier, Universite de Sherbrooke	
 * ---------------------------------------------------------------------------
 * Description	:	Detection de jonction a l'aide de deux Laplaciens de la
 *				:	Gaussienne.  
 * ---------------------------------------------------------------------------
 * Retour    	:	bool
 * ---------------------------------------------------------------------------
 * Notes    	:	Transcription de l'algorithme de S. Tabonne tire de 
 *				:	l'article : Detecting Junction Using Laplacian of Gaussian 
 *				:	Operator, 12th Internationnal Conference on Pattern
 *				:	Recognition, p.52-56, 1994
 * ---------------------------------------------------------------------------
 * Modifications:	
 *****************************************************************************/
bool TabonneCornerDetect
	(
	Image&			o_imgCorner,		// image resultante 
	const Image& 	i_img,				// image source
	double			i_dLowScale, 		// Basse echelle
	double			i_dHighScale,		// Haute echelle
	double			i_dLowThresh, 		// Seuil pour basse echelle
	double			i_dHighThresh		// Seuil pour haute echelle
	)
{
	int		nWidth(i_img.GetWidth()),
			nHeight(i_img.GetHeight()),
			nBands(i_img.GetNumBands()),
			iHigh, jHigh, iLow, jLow,	// coordonnees des extrema
			izc, jzc;					// coordonnees des passages par zero
			
	float 	iLowSub, jLowSub, 
				iHighSub, jHighSub;		// coordonnee subpixel
	

	Image	zc(nWidth, nHeight), 			// passages par zero a basse echelle
			lowLap(nWidth, nHeight), 		// laplacien a basse echelle
			highLap(nWidth, nHeight), 		// laplacien a haute echelle
			Ixx(nWidth, nHeight), Iyy(nWidth, nHeight), Ixy(nWidth, nHeight), 
			Iy(nWidth, nHeight), Ix(nWidth, nHeight), 
			lowExtrema(nWidth, nHeight),	// extrema a basse echelle
			highExtrema(nWidth, nHeight),	// extrema a haute echelle
			Hessian(nWidth, nHeight);

	Bande	dgmask, gmask, ddgmask;				// masques gaussiens
			
	vector<double> Theta(2);		// vecteur de l'orientation du gradient
	
	if (i_dLowScale >= i_dHighScale)
		// Invalid scales
		return false;
			
	if (!o_imgCorner.Resize(nWidth, nHeight, 1))
		// Resizing output image failed
		return false;
	
	// calcul des derivees a basse echelle
	gaussMask1D(gmask, i_dLowScale);

	gaussDerivMask1D(dgmask, i_dLowScale);
	
	gaussSecondDerivMask1D(ddgmask, i_dLowScale);
	
	if (!convolution1D(Ixx, i_img, ddgmask, gmask))
		// Convolution failure
		return false;
	
	if (!convolution1D(Iyy, i_img, gmask, ddgmask))
		// Convolution failure
		return false;
	
	if (!convolution1D(Ixy, i_img, dgmask, dgmask))
		// Convolution failure
		return false;
	
	Hessian = Ixx * Iyy - Ixy * Ixy;

	lowLap = Ixx + Iyy;

	// calcul des extremum a basse echelle
	if (!SearchExtremum(lowExtrema, lowLap, Hessian, i_dLowThresh))
		// Searching for local extrema - internal failure
		return false;
		
	// calcul des passages par zero a basse echelle
	if (!ZeroCrossings(zc, lowLap))
		// Searching for zero crossings - internal failure
		return false;
	
	// calcul des derivees a haute echelle
	gaussMask1D(gmask, i_dHighScale);
		
	gaussDerivMask1D(dgmask, i_dHighScale);
	
	gaussSecondDerivMask1D(ddgmask, i_dHighScale);
	
	if (!convolution1D(Ixx, i_img, ddgmask, gmask))
		// Convolution failure
		return false;
	
	if (!convolution1D(Iyy, i_img, gmask, ddgmask))
		// Convolution failure
		return false;
	
	if (!convolution1D(Ixy, i_img, dgmask, dgmask))
		// Convolution failure
		return false;
	
	Hessian = Ixx * Iyy - Ixy * Ixy;

	highLap = Ixx + Iyy;

	// calcul des extremum a haute echelle
	if (!SearchExtremum(highExtrema, highLap, Hessian, i_dHighThresh))
		// Searching for local extrema - internal failure
		return false;

	// calcul des premieres derivee a haute echelle
	if (!convolution1D(Ix, i_img, dgmask, gmask))
		// Convolution failure
		return false;
	
	if (!convolution1D(Iy, i_img, gmask, dgmask))
		// convolution failure
		return false;

	o_imgCorner.InitWith(0);
	Image tmp(i_img);
	
	// recherche des jonctions subpixel
	for (int b=0; b<nBands; b++){
		for (iHigh = 1; iHigh < nWidth-1; iHigh++)
			for (jHigh = 1; jHigh < nHeight-1; jHigh++){
				if (highExtrema[b](iHigh, jHigh)) {			
					// recherche de la position subpixel de l'extrema
					Tchebitcheff3x3(iHighSub, jHighSub, highLap[b], iHigh, jHigh);

					// mise en correpondance avec le calcul du gradient
					Theta[0] = Ix[b](iHigh, jHigh);
					Theta[1] = Iy[b](iHigh, jHigh);
					Normalize(Theta);
					
					MiseEnCorresp(iLow, jLow, lowLap[b], highLap[b], lowExtrema[b], 
									iHigh, jHigh, Theta);
								
					if ((iLow != NOTFOUND) && (jLow != NOTFOUND)) 
					{					
						// recherche de la position subpixel de l'extrema
						Tchebitcheff3x3(iLowSub, jLowSub, lowLap[b], iLow, jLow);
						
						if ((iHighSub != iLowSub) || (jHighSub != jLowSub)) 
						{
							// recherche du passage par zero correspondant a la
							// droite passant par (iHighSub, jHighSub) et
							// (iLowSub, jLowSub)
							FindZeroCross(izc, jzc, zc[b], iHighSub, jHighSub, 
											iLowSub, jLowSub); 

							if ((izc != NOTFOUND) && (jzc != NOTFOUND)) 
							{
								o_imgCorner[b](izc, jzc) = 255;
								lowExtrema[b](iLow, jLow) = 0;
										
							}
						}
					}
				}

			}
				
/*		for (iHigh = 1; iHigh < nWidth-1; iHigh++)
		for (jHigh = 1; jHigh < nHeight-1; jHigh++){
			if (o_imgCorner[b](iHigh, jHigh)){
				tmp[b](iHigh-1, jHigh-1) = 255;
				tmp[b](iHigh-1, jHigh) = 255;
				tmp[b](iHigh-1, jHigh+1) = 255;
				tmp[b](iHigh, jHigh-1) = 255;
				tmp[b](iHigh, jHigh+1) = 255;
				tmp[b](iHigh+1, jHigh-1) = 255;
				tmp[b](iHigh+1, jHigh) = 255;
				tmp[b](iHigh+1, jHigh+1) = 255;
			}
		}
*/

	}


//	o_imgCorner = tmp;

	return true;
}


/******************************************************************************
 *
 * Fonction  	:	NVFindZeroCross
 * Creee le  	:	novembre 1998
 * Par       	:	Marie-Flavie Auclair-Fortier, Universite de Sherbrooke	
 * ---------------------------------------------------------------------------
 * Description	:	Recherche un passage par zero sur la ligne joignant 
 *				:	deux passages par zero.  
 * ---------------------------------------------------------------------------
 * Retour    	:	bool
 * ---------------------------------------------------------------------------
 * Notes    	:	Transcription de l'algorithme de Tabonne.
 * ---------------------------------------------------------------------------
 * Modifications:	
 *****************************************************************************/
#ifndef INFINITY
	#define	INFINITY 10
#endif

#define SUPBOUND 9

bool FindZeroCross(
	int&			o_nX, 			// coordonnees du passage par zero
	int&			o_nY,			
	const Bande&	i_bandZC, 		// passages par zero
	float			i_fXHigh,		// coordonnees subpixel de l'extrema a haute echelle
	float			i_fYHigh, 	
	float			i_fXLowSub,		// coordonnees subpixel de l'extrema a basse echelle
	float			i_fYLowSub		
)
{
	int nWidth(i_bandZC.GetWidth()), 
		nHeight(i_bandZC.GetHeight()),
		i, j, 
		nbIter(0);

	bool	bFind(false);
	
	vector<double>	vecTheta(2);	// vecteur de l'orientation de la droite
								// passant par les deux extrema
	double	dRho, dR;

	// calcul des parametres de la droite passant 
	// par les deux extrema d'equation rho = i*cos(theta)+j*sin(theta)
	vecTheta[0] = (i_fXLowSub - i_fXHigh);
	vecTheta[1] = (i_fYLowSub - i_fYHigh);
	Normalize(vecTheta);

	dRho = (i_fXLowSub * vecTheta[1]) - (i_fYLowSub * vecTheta[0]);

	if (IsZero(vecTheta[0])) 
		dR = INFINITY;				// la tg est infinie ds ce cas 
	else 
		dR = vecTheta[1] / vecTheta[0];
		
	// dans le premier secteur cone entre -Pi/4 et Pi/4 
	if ((dR >= - 1.) && (dR <= 1.)) {

		if ((i_fXLowSub - i_fXHigh) < 0.0) {
			// decrementer i 
			for (i = (int) (i_fXLowSub+1.);
				 (!bFind) && (nbIter < SUPBOUND); i--, nbIter++){
				j  = (int) (((- dRho + i*vecTheta[1]) / vecTheta[0]) + 0.5);

				if ((i>=0 && i<nWidth && j>=0 && j<nHeight) && i_bandZC(i, j)){
					bFind = 1; 
					o_nX = i; 
					o_nY = j;
				}
			}
		}
		else {
			// incrementer i 
			for (i = (int) (i_fXLowSub+1.);
				 (!bFind) && (nbIter < SUPBOUND); i++, nbIter++) {
				j = (int) (((- dRho + i*vecTheta[1]) / vecTheta[0]) + 0.5);

				if ((i>=0 && i<nWidth && j>=0 && j<nHeight) && i_bandZC(i, j)) {
					bFind = 1;
					o_nX = i; 
					o_nY = j;
				}
			}
		}

	}
	
	// ds le 2eme secteur 
	else {
	
		if ((i_fYLowSub - i_fYHigh) < 0.0) {
			// decrementer i 
			for (j = (int) (i_fYLowSub+1.);
				(!bFind) && (nbIter < SUPBOUND); j--, nbIter++) {
				i  = (int) (((dRho + j*vecTheta[0]) / vecTheta[1]) + 0.5);

				if ((i>=0 && i<nWidth && j>=0 && j<nHeight) && i_bandZC(i, j)) {
					bFind = 1;
					o_nX = i; 
					o_nY = j;
				}
			}
		}
		else {
			// incrementer i 
			for (j = (int) (i_fYLowSub+1.);
				 (!bFind) && (nbIter < SUPBOUND);j++, nbIter++) {
				i  = (int) (((dRho + j*vecTheta[0]) / vecTheta[1]) + 0.5);

				if ((i>=0 && i<nWidth && j>=0 && j<nHeight) && i_bandZC(i, j)) {
					bFind = 1; 
					o_nX = i; 
					o_nY = j;
				}
			}
		}

	}
	
	if (!bFind) { 
		o_nX = NOTFOUND; 
		o_nY = NOTFOUND;
	}
	
	return true;

}


inline double dotProduct(vector<double> V1, vector<double> V2){
	return V1[0]*V2[0] + V1[1]*V2[1];
}

/******************************************************************************
 *
 * Fonction  	:	NVMiseEnCorresp
 * Creee le  	:	novembre 1998
 * Par       	:	Marie-Flavie Auclair-Fortier, Universite de Sherbrooke	
 * ---------------------------------------------------------------------------
 * Description	:	Fonction de recherche d'un correspondant d'un extremum local. 
 *				:	La recherche se fait dans un vois 7x7.  
 * ---------------------------------------------------------------------------
 * Retour    	:	bool
 * ---------------------------------------------------------------------------
 * Notes    	:	Transcription de l'algorithme de Tabonne.
 * ---------------------------------------------------------------------------
 * Modifications:	
 *****************************************************************************/
bool MiseEnCorresp(
	int&			o_nX,					// coordonnees a basse echelle
	int&			o_nY,		
	const Bande&	i_imgLow,				// Laplacien basse echelle
	const Bande&	i_imgHigh, 				// Laplacien haute echelle
	const Bande&	i_imgExtrema, 			// extrema basse echelle
	int				i_nX,					// coordonnees a haute echelle
	int				i_nY, 		
	vector<double>& i_vecTheta				// vecteur du gradient
)
{
	int		li, lj, k, l,		// var temporaires
			nVois, 				// voisinage
			nWidth(i_imgLow.GetWidth()),
			nHeight(i_imgLow.GetHeight());
			
	bool	bFind(false);

	vector<double>	V1(2), V2(2);
	
	o_nX = NOTFOUND; 
	o_nY = NOTFOUND;
 
	// l'inverse du vecteur gradient 
	if (i_imgHigh(i_nX, i_nY) < 0.0){ 
		i_vecTheta[0] = - i_vecTheta[0]; 
		i_vecTheta[1] = - i_vecTheta[1];
	}

	// recherche ds voisinage 3x3 
	// les mauvaises mises en correpondances ds vois 5x5 
	// les mauvaises mises en correpondances ds vois 7x7 
	for (nVois= 1; nVois<4; nVois++) // differents voisinage
		for (li = -nVois; li<= nVois;li++)
			for (lj = -nVois; lj<=nVois; lj++)
				if( nVois== 1 || (abs(li) == nVois) || (abs(lj) == nVois)) {
					k = i_nX + li;
					l = i_nY + lj;
					if ((k >= 0 && k < nWidth && l >= 0 && l < nHeight) &&
						i_imgExtrema(k, l) && 
						(i_imgLow(k, l)*i_imgHigh(i_nX, i_nY) > 0.0) &&
						IsSupTo(fabs((double) i_imgLow(k, l)), fabs((double) i_imgHigh(i_nX, i_nY))) ) {
						if (!bFind) {
							o_nX = k; 
							o_nY = l;
							bFind = true;
						}
						else {
							V1[0] = li;
							V1[1] = lj;
							V2[0] = (o_nX - i_nX);
							V2[1] = (o_nY - i_nY);
							
							Normalize(V1);
							Normalize(V2);

							if (dotProduct(V1, i_vecTheta) > dotProduct(V2, i_vecTheta)) {
								o_nX = k; 
								o_nY = l;
							} 
						}
					}
				}

	return true;
}

#define unSneuf	1./9.
#define unSsix  1./6.

/******************************************************************************
 *
 * Fonction  	:	NVTChebitcheff3x3
 * Creee le  	:	novembre 1998
 * Par       	:	Marie-Flavie Auclair-Fortier, Universite de Sherbrooke	
 * ---------------------------------------------------------------------------
 * Description	:	Recherche de maniere subpixel la position du maximum a
 *				:	l'aide d'un calcul des coefficients du polynome de   
 *				:	tchebitcheff dans un voisinage 3x3 centre sur le point 
 *				:	de coord (i, j).
 * ---------------------------------------------------------------------------
 * Retour    	:	bool
 * ---------------------------------------------------------------------------
 * Notes    	:	Transcription de l'algorithme de Tabonne.
 * ---------------------------------------------------------------------------
 * Modifications:	
 *****************************************************************************/
bool Tchebitcheff3x3(
	float&			o_fSubX,	// coordonnees subpixel de la position du maximum
	float&			o_fSubY,	
	const Bande&	i_imgLap,		// image du laplacien
	int				i,			// coordonnees en entree
	int				j			
	)
{
	float b0, b1, b2, b3, b4, b5;
	
	b0 = (float)((unSneuf)*(i_imgLap(i-1,j-1) + i_imgLap(i,j-1) + i_imgLap(i+1,j-1) 
		+ i_imgLap(i-1,j) + i_imgLap(i,j) + i_imgLap(i+1,j) + i_imgLap(i-1,j+1) 
		+ i_imgLap(i,j+1) + i_imgLap(i+1,j+1)));
		
	b1 = (float)((unSsix)*(- i_imgLap(i-1,j-1)- i_imgLap(i,j-1) - i_imgLap(i+1,j-1) 
		+ i_imgLap(i-1,j+1) + i_imgLap(i,j+1) + i_imgLap(i+1,j+1)));
		
	b2 = (float)((unSsix)*(- i_imgLap(i-1,j-1) - i_imgLap(i-1,j) - i_imgLap(i-1,j+1)
		+ i_imgLap(i+1,j-1) + i_imgLap(i+1,j) + i_imgLap(i+1,j+1)));
		
	b3 = (float)((unSsix)*(i_imgLap(i-1,j-1) + i_imgLap(i,j-1) + i_imgLap(i+1,j-1)
		+ i_imgLap(i-1,j+1) + i_imgLap(i,j+1) + i_imgLap(i+1,j+1) 
		- 2.0*(i_imgLap(i-1,j) + i_imgLap(i,j) + i_imgLap(i+1,j))));
		
	b4 = (float)((0.25)*(i_imgLap(i-1,j-1) - i_imgLap(i+1,j-1) - i_imgLap(i-1,j+1) 
		+ i_imgLap(i+1,j+1)));
		
	b5 = (float)((unSsix)*(i_imgLap(i-1,j-1) + i_imgLap(i-1,j) + i_imgLap(i-1,j+1)
		+ i_imgLap(i+1,j-1) + i_imgLap(i+1,j) + i_imgLap(i+1,j+1)
		- 2.0*(i_imgLap(i,j-1) + i_imgLap(i,j) + i_imgLap(i,j+1))));
		
	// coordonnees subpixel du maximum  
	o_fSubX = (float)(i - (-b4*b2 + (2.*b5*b1))/(-b4*b4 + (4.*b5*b3)));
	o_fSubY = (float)(j + (b1*b4 - (2.*b3*b2))/(-b4*b4 + (4.*b5*b3)));

	if (sqrt((double) ((i-(o_fSubX))*(i-(o_fSubX)) 
		+ (j-(o_fSubY))*(j-(o_fSubY)))) > 1.) {
		o_fSubX = (float) i; 
		o_fSubY = (float) j;
	}
	
	return true;

}


/******************************************************************************
 *
 * Fonction  	:	NVIsExtremumMax
 * Creee le  	:	novembre 1998
 * Par       	:	Marie-Flavie Auclair-Fortier, Universite de Sherbrooke	
 * ---------------------------------------------------------------------------
 * Description	:	Procedure qui retourne vrai si un point (x,y) est un
 *				:	maximum local.
 * ---------------------------------------------------------------------------
 * Retour    	:	bool
 * ---------------------------------------------------------------------------
 * Notes    	:	
 * ---------------------------------------------------------------------------
 * Modifications:	
 *****************************************************************************/
bool IsExtremumMax(
	const Bande&	i_band,
	int				x, 
	int				y
)
{
	int i, j;
	
	for (i =- 1; i <= 1; i++)
		for (j =- 1; j <= 1; j++)
			if (i_band(x, y) < i_band(x+i, y+j))
				return false;

	return true;

}

/******************************************************************************
 *
 * Fonction  	:	NVIsExtremumMin
 * Creee le  	:	novembre 1998
 * Par       	:	Marie-Flavie Auclair-Fortier, Universite de Sherbrooke	
 * ---------------------------------------------------------------------------
 * Description	:	Procedure qui retourne vrai si un point (x,y) est un
 *				:	minimum local.
 * ---------------------------------------------------------------------------
 * Retour    	:	bool
 * ---------------------------------------------------------------------------
 * Notes    	:	
 * ---------------------------------------------------------------------------
 * Modifications:	
 *****************************************************************************/
bool IsExtremumMin(
	const Bande&	in_img,
	int				x, 
	int				y
)
{
	int i, j;
	
	for (i = -1; i <= 1; i++)
		for (j =- 1; j <= 1; j++)
			if (in_img(x, y) > in_img(x+i, y+j))
				return false;

	return true;

}

/******************************************************************************
 *
 * Fonction  	:	NVIsExtremum
 * Creee le  	:	novembre 1998
 * Par       	:	Marie-Flavie Auclair-Fortier, Universite de Sherbrooke	
 * ---------------------------------------------------------------------------
 * Description	:	Procedure qui retourne vrai si un point (x, y) est un 
 *				:	extremum local.
 * ---------------------------------------------------------------------------
 * Retour    	:	bool
 * ---------------------------------------------------------------------------
 * Notes    	:	
 * ---------------------------------------------------------------------------
 * Modifications:	
 *****************************************************************************/
bool IsExtremum(
	const Bande&	i_band, 
	int				x,		// coordonnées du pixel
	int				y
)
{
	if (i_band(x, y) > 0.0)
		return IsExtremumMax(i_band, x, y);
	else 
		return IsExtremumMin(i_band, x, y); 

}

  
/******************************************************************************
 *
 * Fonction  	:	NVSearchExtremum
 * Creee le  	:	novembre 1998
 * Par       	:	Marie-Flavie Auclair-Fortier, Universite de Sherbrooke	
 * ---------------------------------------------------------------------------
 * Description	:	Recherche un extremum hyperbolique (dans toutes les
 *				:	directions) et seuillage.
 * ---------------------------------------------------------------------------
 * Retour    	:	bool
 * ---------------------------------------------------------------------------
 * Notes    	:	
 * ---------------------------------------------------------------------------
 * Modifications:	
 *****************************************************************************/
bool SearchExtremum(
	Image& 			o_img,
	const Image&	i_imgLap,
	const Image&	i_imgHessian,
	double			i_dThresh
)	
{
	int i, j,
		nWidth(i_imgLap.GetWidth()),
		nHeight(i_imgLap.GetHeight()),
		nBands(i_imgLap.GetNumBands());
	
	if (!o_img.Resize(nWidth, nHeight, nBands))
		return false;
		
	o_img.InitWith(0);
	
	for (int b=0; b<nBands; b++){
		for (i = 1; i < nWidth-1; i++) 
			for (j = 1; j < nHeight-1; j++){
				if (IsExtremum(i_imgLap[b], i, j) &&
					((i_imgHessian[b](i, j)) > ((PIXTYPE) (fabs(i_imgLap[b](i, j)))*i_dThresh))){
					o_img[b](i, j) = 255;  
				}

			}
	}
			
	return true;

}

