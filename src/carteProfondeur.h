#include <string>
#include <vector>
#include <map>
#include <iostream>
#include "image.h"

#define SIZE_MIN 32

struct Coord
{
	int x;
	int y;
	Coord operator/(const double& val) const
	{
		return Coord{x/val, y/val};
	}
};

class carteProfondeur 
{
public:
	carteProfondeur();
	~carteProfondeur();
	Image getCarteProfondeur(Image& nom_image_gauche,Image& nom_image_droite);

private:
	// methodes
	void genererHierarchie();
	Image resize(Image& in_image, int width, int height);
	Coord miseEnCorrespondance(Coord pixel_gauche); // Methode hierarchique
	Coord correlation(Coord pixel_gauche, Coord region_droite, int in_niveau);
	float similarite_fenetre(Coord& coord_gauche, Coord& coord_droite, const Image& imgGauche, const Image& imgDroite);
	float similarite(Coord& coord_gauche, Coord& coord_droite, const Image& imgGauche, const Image& imgDroite);


	// membres
	Image m_ImGauche;
	Image m_ImDroite;
	std::vector<Image> m_HierarchieGauche;
	std::vector<Image> m_HierarchieDroite;
};